function message(parent, args, context) {
	return context.prisma.replyMessage({
		id: parent.id
	}).message();
}

module.exports = {
	message
}