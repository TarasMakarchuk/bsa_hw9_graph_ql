function postMessage(parent, args, context, info) {
	return context.prisma.createMessage({
		text: args.text,
		likeCount: 0,
		dislikeCount: 0,
	});
}

async function postReplyMessage(parent, args, context, info) {
	const messageExists = await context.prisma.$exists.message({
		id: args.messageId
	});

	if(!messageExists) {
		throw new Error(`Message with ID ${args.messageId} does not exist`);
	}

	return context.prisma.createReplyMessage({
		text: args.text,
		message: { connect: { id: args.messageId }}
	});
}

module.exports = {
	postMessage,
	postReplyMessage
}