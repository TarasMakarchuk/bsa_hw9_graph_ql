import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
    query productQuery($sortedBy: MessagesSortBy) {
        messages(sortedBy: $sortedBy) {
            count
            messageList {
                id
                text
                likeCount
                dislikeCount
                replies {
                    id
                    text
                }
            }
        }
    }
`;

export const POST_MESSAGE_MUTATION = gql`
    mutation PostMutation($text: String!) {
        postMessage(text: $text) {
            id
            text
            replies {
                id
                text
            }
        }
    }
`;

export const POST_REPLY_MUTATION = gql`
    mutation PostMutation($messageId: ID!, $text: String!) {
        postReplyMessage(messageId: $messageId, text: $text) {
            id
            text
        }
    }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
    subscription {
        newMessage {
            id
            text
            replies {
                id
                text
            }
        }
    }
`;
