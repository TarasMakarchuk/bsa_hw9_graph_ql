import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {WebSocketLink} from "apollo-link-ws";
import {createHttpLink} from "apollo-link-http";
import {split} from "apollo-link";
import {getMainDefinition} from "apollo-utilities";
import ApolloClient from "apollo-client";
import {InMemoryCache} from "apollo-cache-inmemory";
import {ApolloProvider} from 'react-apollo';
import {BrowserRouter} from "react-router-dom";

const wsLink = new WebSocketLink({
	uri: `ws://localhost:4000`,
	options: {
		reconnect: true
	}
});

const httpLink = createHttpLink({
	uri: `http://localhost:4000`,
});


const link = split(
	({ query}) => {
		const { kind, operation } = getMainDefinition(query);
		return kind === "OperationDefinition" && operation === 'subscription';
	},
	wsLink,
	httpLink,
);

const client = new ApolloClient({
	link,
	cache: new InMemoryCache(),
});

ReactDOM.render(
	<React.StrictMode>
		<BrowserRouter>
			<ApolloProvider client={client}>
				<App/>
			</ApolloProvider>
		</BrowserRouter>
	</React.StrictMode>,
	document.getElementById('root')
);
