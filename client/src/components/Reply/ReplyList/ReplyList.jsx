import React from "react";
import ReplyItem from "../RpelyItem/ReplyItem";
import ReplyForm from "../ReplyForm/ReplyForm";

const ReplyList = props => {
	const [showReplyForm, toggleForm] = React.useState(false);
	const { messageId, replies } = props;

	return (
		<div className='reply-list'>
			{replies.length > 0 && <span className='reply-list-title'>replies</span>}
			{replies.map(item => <ReplyItem key={item.id} {...item}/>)}
			<button className='reply-button' onClick={()=> toggleForm(!showReplyForm)}>
				{showReplyForm ? 'Close reply' : 'Add reply'}
			</button>
			{ showReplyForm && <ReplyForm
				messageId={messageId}
				toggleForm={toggleForm}
			/>}
		</div>
	)
}

export default ReplyList;