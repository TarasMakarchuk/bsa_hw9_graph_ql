import React from "react";

const ReplyItem = props => {
	const { text } = props;
	return (
		<div>
			<p>{text}</p>
		</div>
	)
}

export default ReplyItem;