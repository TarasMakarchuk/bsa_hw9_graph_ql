import React, {useState} from "react";
import {Mutation} from 'react-apollo';
import {POST_REPLY_MUTATION, MESSAGE_QUERY} from "../../../queries";

const ReplyForm = props => {
	const { messageId, toggleForm } = props;
	const [text, setText] = useState('');

	const _updateStoreAfterAddingReply = (store, newReply, messageId) => {
		const sortedBy = 'createdAt_DESC';

		const data = store.readQuery({
			query: MESSAGE_QUERY,
			variables: {
				sortedBy
			}
		});

		const replyMessage = data.messages.messageList.find(
			item => item.id === messageId
		);

		replyMessage.replies.push(newReply);
		store.writeQuery({ query: MESSAGE_QUERY, data });
		toggleForm(false);
	}

	return (
		<div className='form-wrapper'>
			<div className='input-wrapper'>
				<textarea
					onChange={e => setText(e.target.value)}
					placeholder='Reply text'
					autoFocus
					value={text}
					cols="30"
				/>
			</div>

			<Mutation
				mutation={POST_REPLY_MUTATION}
				variables={{messageId, text}}
				update={(store, {data: {messageReply}}) => {
					_updateStoreAfterAddingReply(store, messageReply, messageId)
				}}
			>
				{postMutation => <button onClick={postMutation}>Reply</button>}
			</Mutation>
		</div>
	)
}

export default ReplyForm;