import React from "react";
import ReplyList from "../../Reply/ReplyList/ReplyList";
import './messageItem.css';

const MessageItem = props => {
	const { id, text, replies } = props;

	let newId = id;
	newId = newId.substr(22, 3);

	return (
		<div className='product-item'>
			<div className='title-wrapper'>
				<span >#{ newId } </span>
				<h3>{ text }</h3>
			</div>
			<ReplyList messageId={ id } replies={ replies }/>
		</div>
	)
}

export default MessageItem;