import React from 'react';
import MessageList from "./components/Message/MessageList/MessageList";
import MessageForm from "./components/Message/MessageForm/messageForm";
import './app.css';

function App() {

  return (
    <div className="App">
      <MessageList />
      <MessageForm />
    </div>
  );
}

export default App;
